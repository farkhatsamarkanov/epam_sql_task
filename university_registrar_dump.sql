PGDMP         3                x            university_registrar    10.13    12.3 6    \           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            ]           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            ^           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            _           1262    16406    university_registrar    DATABASE     �   CREATE DATABASE university_registrar WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252' TABLESPACE = university_registrar_tablespace;
 $   DROP DATABASE university_registrar;
                postgres    false                        3079    16534    pgstattuple 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS pgstattuple WITH SCHEMA public;
    DROP EXTENSION pgstattuple;
                   false            `           0    0    EXTENSION pgstattuple    COMMENT     C   COMMENT ON EXTENSION pgstattuple IS 'show tuple-level statistics';
                        false    2            �            1255    16522 $   get_max_semester_for_course(integer)    FUNCTION     e  CREATE FUNCTION public.get_max_semester_for_course(input_course_id integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
declare semester_id_to_return text;

BEGIN
select array_to_string(array_agg(second_ss.semester_id), '') into semester_id_to_return from
(select ss.semester_id, MAX(ss.count) as max_times_in_single_semester
FROM
(SELECT schedule_of_classes.semester_id, count(schedule_of_classes.semester_id) as "count"
FROM schedule_of_classes
WHERE course_id = input_course_id
group by semester_id
order by count desc) as ss
group by semester_id
limit 1) as second_ss;

return semester_id_to_return;
end;
$$;
 K   DROP FUNCTION public.get_max_semester_for_course(input_course_id integer);
       public          postgres    false            �            1255    16523 5   get_most_picked_semester_for_specific_course(integer)    FUNCTION     �  CREATE FUNCTION public.get_most_picked_semester_for_specific_course(input_course integer) RETURNS TABLE(course_id integer, semester_id character, max_times_in_single_semester bigint)
    LANGUAGE plpgsql
    AS $$BEGIN
	RETURN QUERY
	select ss.course_id_1, ss.semester_id_1, MAX(ss.count) as max_times_in_single_semester
	FROM
	(SELECT schedule_of_classes.course_id as course_id_1, schedule_of_classes.semester_id as semester_id_1, count(schedule_of_classes.semester_id) as "count"
	FROM schedule_of_classes
	WHERE schedule_of_classes.course_id = input_course
	group by semester_id_1, course_id_1
	order by count desc) as ss
	group by semester_id_1, course_id_1
	limit 1;
END
$$;
 Y   DROP FUNCTION public.get_most_picked_semester_for_specific_course(input_course integer);
       public          postgres    false            �            1255    16533    get_most_picked_semesters()    FUNCTION       CREATE FUNCTION public.get_most_picked_semesters() RETURNS TABLE(course_id integer, semester_id character, max_times_in_single_semester bigint)
    LANGUAGE plpgsql
    AS $$DECLARE
   elem integer;
BEGIN
   FOR elem IN
      SELECT courses.course_id FROM courses
    
   LOOP
      RETURN QUERY SELECT * from get_most_picked_semester_for_specific_course(elem);
   END LOOP;
END
$$;
 2   DROP FUNCTION public.get_most_picked_semesters();
       public          postgres    false            �            1255    16480 L   list_courses_for_lecturer_and_semester(character varying, character varying)    FUNCTION     �  CREATE FUNCTION public.list_courses_for_lecturer_and_semester(lecturer_name character varying, input_semester_id character varying) RETURNS text
    LANGUAGE sql
    AS $$

	SELECT array_to_string(array_agg(course_id), ', ')
	FROM schedule_of_classes
	LEFT JOIN lecturers ON schedule_of_classes.lecturer_id = lecturers.lecturer_id
	WHERE name = lecturer_name AND semester_id = input_semester_id;
    
$$;
 �   DROP FUNCTION public.list_courses_for_lecturer_and_semester(lecturer_name character varying, input_semester_id character varying);
       public          postgres    false            �            1255    16476    log_insert_academic_ranks()    FUNCTION     W  CREATE FUNCTION public.log_insert_academic_ranks() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'academic_ranks',
CONCAT('numeric_rank: "', NEW.numeric_rank, '";', ' name: "', TRIM(NEW.name), '"'));
RETURN NEW;
END;
$$;
 2   DROP FUNCTION public.log_insert_academic_ranks();
       public          postgres    false            �            1255    16474    log_insert_courses()    FUNCTION     �  CREATE FUNCTION public.log_insert_courses() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'courses',
CONCAT('course_id: "', NEW.course_id, '";', ' course_title: "', TRIM(NEW.course_title), '";', ' course_description: "', TRIM(NEW.course_description), '"'));
RETURN NEW;
END;
$$;
 +   DROP FUNCTION public.log_insert_courses();
       public          postgres    false            �            1255    16472    log_insert_lecturers()    FUNCTION     �  CREATE FUNCTION public.log_insert_lecturers() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'lecturers',
CONCAT('lecturer_id: "', NEW.lecturer_id, '";', ' name: "', TRIM(NEW.name), '";', ' date_of_birth: "', NEW.date_of_birth, '";', ' numeric_academic_rank: "', NEW.numeric_academic_rank, '"'));
RETURN NEW;
END;
$$;
 -   DROP FUNCTION public.log_insert_lecturers();
       public          postgres    false            �            1255    16470     log_insert_schedule_of_classes()    FUNCTION     �  CREATE FUNCTION public.log_insert_schedule_of_classes() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'schedule_of_classes',
CONCAT('student_id: "', NEW.student_id, '";', ' lecturer_id: "', NEW.lecturer_id, '";', ' course_id: "', NEW.course_id, '";', ' time: "', NEW.time, '";', ' location: "', TRIM(NEW.location), '";', ' semester_id: "', NEW.semester_id, '"'));
RETURN NEW;
END;
$$;
 7   DROP FUNCTION public.log_insert_schedule_of_classes();
       public          postgres    false            �            1255    16459    log_insert_semesters()    FUNCTION     �  CREATE FUNCTION public.log_insert_semesters() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'semesters',
CONCAT('semester_id: "', NEW.semester_id, '";', ' name: "', TRIM(NEW.name), '";', ' year: "', NEW.year, '";', ' start: "', NEW.start_time, '";', ' end: "', NEW.end_time, '"'));
RETURN NEW;
END;
$$;
 -   DROP FUNCTION public.log_insert_semesters();
       public          postgres    false            �            1255    16465    log_insert_students()    FUNCTION     u  CREATE FUNCTION public.log_insert_students() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'students',
CONCAT('student_id: "', NEW.student_id, '";', ' name: "', TRIM(NEW.name), '";', ' date_of_birth: "', NEW.date_of_birth, '"'));
RETURN NEW;
END;
$$;
 ,   DROP FUNCTION public.log_insert_students();
       public          postgres    false            �            1255    16467    log_insert_users()    FUNCTION     �  CREATE FUNCTION public.log_insert_users() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ BEGIN
INSERT INTO public.logging
(record_insert_datetime,
referenced_table_name,
description)
VALUES (CURRENT_TIMESTAMP,
'users',
CONCAT('login: "', TRIM(NEW.login), '";', ' password: "', TRIM(NEW.password), '";', ' is_active: "', NEW.is_active, '";', ' student_id: "', NEW.student_id, '"'));
RETURN NEW;
END;
$$;
 )   DROP FUNCTION public.log_insert_users();
       public          postgres    false            �            1259    16412    academic_ranks    TABLE     b   CREATE TABLE public.academic_ranks (
    numeric_rank integer NOT NULL,
    name character(45)
);
 "   DROP TABLE public.academic_ranks;
       public            postgres    false            �            1259    16442    schedule_of_classes    TABLE     �   CREATE TABLE public.schedule_of_classes (
    student_id integer,
    lecturer_id integer,
    course_id integer,
    "time" integer,
    location character(45),
    semester_id character(7)
);
 '   DROP TABLE public.schedule_of_classes;
       public            postgres    false            �            1259    16437    users    TABLE     �   CREATE TABLE public.users (
    login character(45) NOT NULL,
    password character(45),
    is_active boolean,
    student_id integer
);
    DROP TABLE public.users;
       public            postgres    false            �            1259    16493    course_inactiveusersnumber    VIEW     �  CREATE VIEW public.course_inactiveusersnumber AS
 SELECT ss.course_id,
    count(ss.student_id) AS count
   FROM ( SELECT schedule_of_classes.course_id,
            schedule_of_classes.student_id,
            users.is_active
           FROM (public.schedule_of_classes
             LEFT JOIN public.users ON ((schedule_of_classes.student_id = users.student_id)))) ss
  WHERE (ss.is_active = false)
  GROUP BY ss.course_id;
 -   DROP VIEW public.course_inactiveusersnumber;
       public          postgres    false    202    203    203    202            �            1259    16417 	   lecturers    TABLE     �   CREATE TABLE public.lecturers (
    lecturer_id integer NOT NULL,
    name character(45),
    date_of_birth integer,
    numeric_academic_rank integer
);
    DROP TABLE public.lecturers;
       public            postgres    false            �            1259    16481    course_lecturer_rank    VIEW       CREATE VIEW public.course_lecturer_rank AS
 SELECT schedule_of_classes.course_id,
    schedule_of_classes.lecturer_id,
    lecturers.numeric_academic_rank
   FROM (public.schedule_of_classes
     JOIN public.lecturers ON ((schedule_of_classes.lecturer_id = lecturers.lecturer_id)));
 '   DROP VIEW public.course_lecturer_rank;
       public          postgres    false    199    203    203    199            �            1259    16485    course_maxlecturerrank    VIEW     �   CREATE VIEW public.course_maxlecturerrank AS
 SELECT course_lecturer_rank.course_id,
    max(course_lecturer_rank.numeric_academic_rank) AS max_rank
   FROM public.course_lecturer_rank
  GROUP BY course_lecturer_rank.course_id;
 )   DROP VIEW public.course_maxlecturerrank;
       public          postgres    false    205    205            �            1259    16489    course_maxlecturerrank_rankname    VIEW     0  CREATE VIEW public.course_maxlecturerrank_rankname AS
 SELECT course_maxlecturerrank.course_id,
    course_maxlecturerrank.max_rank,
    academic_ranks.name
   FROM (public.course_maxlecturerrank
     LEFT JOIN public.academic_ranks ON ((course_maxlecturerrank.max_rank = academic_ranks.numeric_rank)));
 2   DROP VIEW public.course_maxlecturerrank_rankname;
       public          postgres    false    198    198    206    206            �            1259    16497 (   course_maxlecturerrank_lecturer_rankname    VIEW     �  CREATE VIEW public.course_maxlecturerrank_lecturer_rankname AS
 SELECT course_maxlecturerrank_rankname.course_id,
    course_maxlecturerrank_rankname.max_rank,
    course_lecturer_rank.lecturer_id,
    course_maxlecturerrank_rankname.name
   FROM (public.course_maxlecturerrank_rankname
     LEFT JOIN public.course_lecturer_rank ON (((course_maxlecturerrank_rankname.course_id = course_lecturer_rank.course_id) AND (course_maxlecturerrank_rankname.max_rank = course_lecturer_rank.numeric_academic_rank))));
 ;   DROP VIEW public.course_maxlecturerrank_lecturer_rankname;
       public          postgres    false    207    205    207    205    205    207            �            1259    16407    courses    TABLE     �   CREATE TABLE public.courses (
    course_id integer NOT NULL,
    course_title character(45),
    course_description character(60)
);
    DROP TABLE public.courses;
       public            postgres    false            �            1259    16549    get_table_sizes    VIEW     �  CREATE VIEW public.get_table_sizes AS
 SELECT c.relname AS relation,
    pg_size_pretty(pg_total_relation_size((c.oid)::regclass)) AS total_size
   FROM (pg_class c
     LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace)))
  WHERE ((n.nspname <> ALL (ARRAY['pg_catalog'::name, 'information_schema'::name])) AND (c.relkind <> 'i'::"char") AND (n.nspname !~ '^pg_toast'::text))
  ORDER BY (pg_total_relation_size((c.oid)::regclass)) DESC;
 "   DROP VIEW public.get_table_sizes;
       public          postgres    false            �            1259    16554    get_tablespace_size    VIEW     �   CREATE VIEW public.get_tablespace_size AS
 SELECT pg_size_pretty(pg_tablespace_size('university_registrar_tablespace'::name)) AS pg_size_pretty;
 &   DROP VIEW public.get_tablespace_size;
       public          postgres    false            �            1259    16447    logging    TABLE     �   CREATE TABLE public.logging (
    record_insert_datetime timestamp(2) with time zone,
    referenced_table_name character(45),
    description text
);
    DROP TABLE public.logging;
       public            postgres    false            �            1259    16427 	   semesters    TABLE     �   CREATE TABLE public.semesters (
    semester_id character(7) NOT NULL,
    name character(30),
    year integer,
    start_time integer,
    end_time integer
);
    DROP TABLE public.semesters;
       public            postgres    false            �            1259    16432    students    TABLE     u   CREATE TABLE public.students (
    student_id integer NOT NULL,
    name character(45),
    date_of_birth integer
);
    DROP TABLE public.students;
       public            postgres    false            �            1259    16544    task_1    VIEW     -  CREATE VIEW public.task_1 AS
 SELECT courses.course_title AS "Course title",
    count(DISTINCT schedule_of_classes.student_id) AS "Number of Students",
    count(DISTINCT schedule_of_classes.lecturer_id) AS "Number of Lecturers",
    course_maxlecturerrank_lecturer_rankname.name AS "Highest lecturer rank",
    course_inactiveusersnumber.count AS "Inactive users",
    t.semester_id AS "Most picked semester"
   FROM ((((public.courses
     LEFT JOIN public.schedule_of_classes ON ((schedule_of_classes.course_id = courses.course_id)))
     LEFT JOIN public.course_maxlecturerrank_lecturer_rankname ON ((courses.course_id = course_maxlecturerrank_lecturer_rankname.course_id)))
     LEFT JOIN public.course_inactiveusersnumber ON ((courses.course_id = course_inactiveusersnumber.course_id)))
     LEFT JOIN public.get_most_picked_semesters() t(course_id, semester_id, max_times_in_single_semester) ON ((courses.course_id = t.course_id)))
  GROUP BY courses.course_title, course_maxlecturerrank_lecturer_rankname.name, course_inactiveusersnumber.count, t.semester_id;
    DROP VIEW public.task_1;
       public          postgres    false    235    209    209    208    208    203    203    203    197    197            S          0    16412    academic_ranks 
   TABLE DATA           <   COPY public.academic_ranks (numeric_rank, name) FROM stdin;
    public          postgres    false    198   �W       R          0    16407    courses 
   TABLE DATA           N   COPY public.courses (course_id, course_title, course_description) FROM stdin;
    public          postgres    false    197   X       T          0    16417 	   lecturers 
   TABLE DATA           \   COPY public.lecturers (lecturer_id, name, date_of_birth, numeric_academic_rank) FROM stdin;
    public          postgres    false    199   �X       Y          0    16447    logging 
   TABLE DATA           ]   COPY public.logging (record_insert_datetime, referenced_table_name, description) FROM stdin;
    public          postgres    false    204   mY       X          0    16442    schedule_of_classes 
   TABLE DATA           p   COPY public.schedule_of_classes (student_id, lecturer_id, course_id, "time", location, semester_id) FROM stdin;
    public          postgres    false    203   1[       U          0    16427 	   semesters 
   TABLE DATA           R   COPY public.semesters (semester_id, name, year, start_time, end_time) FROM stdin;
    public          postgres    false    200   �[       V          0    16432    students 
   TABLE DATA           C   COPY public.students (student_id, name, date_of_birth) FROM stdin;
    public          postgres    false    201   p\       W          0    16437    users 
   TABLE DATA           G   COPY public.users (login, password, is_active, student_id) FROM stdin;
    public          postgres    false    202   ]       �
           2606    16416 "   academic_ranks academic_ranks_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.academic_ranks
    ADD CONSTRAINT academic_ranks_pkey PRIMARY KEY (numeric_rank);
 L   ALTER TABLE ONLY public.academic_ranks DROP CONSTRAINT academic_ranks_pkey;
       public            postgres    false    198            �
           2606    16411    courses courses_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (course_id);
 >   ALTER TABLE ONLY public.courses DROP CONSTRAINT courses_pkey;
       public            postgres    false    197            �
           2606    16421    lecturers lecturers_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.lecturers
    ADD CONSTRAINT lecturers_pkey PRIMARY KEY (lecturer_id);
 B   ALTER TABLE ONLY public.lecturers DROP CONSTRAINT lecturers_pkey;
       public            postgres    false    199            �
           2606    16431    semesters semesters_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.semesters
    ADD CONSTRAINT semesters_pkey PRIMARY KEY (semester_id);
 B   ALTER TABLE ONLY public.semesters DROP CONSTRAINT semesters_pkey;
       public            postgres    false    200            �
           2606    16436    students students_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.students
    ADD CONSTRAINT students_pkey PRIMARY KEY (student_id);
 @   ALTER TABLE ONLY public.students DROP CONSTRAINT students_pkey;
       public            postgres    false    201            �
           2606    16441    users users_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (login);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    202            �
           2620    16477 $   academic_ranks tr_ins_academic_ranks    TRIGGER     �   CREATE TRIGGER tr_ins_academic_ranks BEFORE INSERT ON public.academic_ranks FOR EACH ROW EXECUTE PROCEDURE public.log_insert_academic_ranks();
 =   DROP TRIGGER tr_ins_academic_ranks ON public.academic_ranks;
       public          postgres    false    198    231            �
           2620    16475    courses tr_ins_courses    TRIGGER     z   CREATE TRIGGER tr_ins_courses BEFORE INSERT ON public.courses FOR EACH ROW EXECUTE PROCEDURE public.log_insert_courses();
 /   DROP TRIGGER tr_ins_courses ON public.courses;
       public          postgres    false    197    230            �
           2620    16473    lecturers tr_ins_lecturers    TRIGGER     �   CREATE TRIGGER tr_ins_lecturers BEFORE INSERT ON public.lecturers FOR EACH ROW EXECUTE PROCEDURE public.log_insert_lecturers();
 3   DROP TRIGGER tr_ins_lecturers ON public.lecturers;
       public          postgres    false    199    229            �
           2620    16471 .   schedule_of_classes tr_ins_schedule_of_classes    TRIGGER     �   CREATE TRIGGER tr_ins_schedule_of_classes BEFORE INSERT ON public.schedule_of_classes FOR EACH ROW EXECUTE PROCEDURE public.log_insert_schedule_of_classes();
 G   DROP TRIGGER tr_ins_schedule_of_classes ON public.schedule_of_classes;
       public          postgres    false    228    203            �
           2620    16460    semesters tr_ins_semesters    TRIGGER     �   CREATE TRIGGER tr_ins_semesters BEFORE INSERT ON public.semesters FOR EACH ROW EXECUTE PROCEDURE public.log_insert_semesters();
 3   DROP TRIGGER tr_ins_semesters ON public.semesters;
       public          postgres    false    226    200            �
           2620    16466    students tr_ins_students    TRIGGER     }   CREATE TRIGGER tr_ins_students BEFORE INSERT ON public.students FOR EACH ROW EXECUTE PROCEDURE public.log_insert_students();
 1   DROP TRIGGER tr_ins_students ON public.students;
       public          postgres    false    201    225            �
           2620    16469    users tr_ins_users    TRIGGER     t   CREATE TRIGGER tr_ins_users BEFORE INSERT ON public.users FOR EACH ROW EXECUTE PROCEDURE public.log_insert_users();
 +   DROP TRIGGER tr_ins_users ON public.users;
       public          postgres    false    227    202            S   G   x�3���+.)*M.�/R ��8}�KR�2���e��X\����X��PP���
����e�@X��=... �z'�      R   �   x����
�0D��W����X�&ţ x����F���7�Xij�<˛�U%:�^�1 ��APK���ђe�\5�4[C��|�%UU�N�&�1٫����V%\�p�'�r�f�7o���
�qmۊ��������C�Yq��c@�d)�{i�9.�����my�|쥔�zq�      T   �   x���A� E��)��r�.l7v�ډ�l �x��@��忧�m&�>r�È���Zi+�0@��8����a�-��#֑��T�W�C,�x�������TTYM�%0EyYv�|xqLk8Q.�5�*�ǜ���'�(�5����� ��m<7      Y   �  x���OK�0 ����X�i�zq ��D�#��lIR�o�k�9�N��M����k^)���c��R��~���J弲�p}DOu���ۛ	���'���j&����h��5ۓ��7%-l[%q^ZCB�T���G��YJy�E��u�*�����h�[���R��Y`[.�����Q[���1a4f�>�Ӷ�|O��L���2�����ү�H1O�"�{Ja溂�3�R����ҹWc�դk*G��ÒvS�y��X}�N;%���@�}PP�M9:2X)�+B��l�kKRǌ$lm�B�u�V:+����*�]wM3���S�1e0�u�~	���7Q�Lzm��i�)�y�k����{��$e$H�=�b��=<���1݊?#���r���;%q�� ��g|�N��ɰ���_��8��x�d��J��C0��Fѿ      X   �   x���1�0Eg�=���qG@TBb�
��pҖ��2<��B���c��㧻SW50�/�������&P�����η9Rh��ێ�Q�e�V�Z�$����o�!BJ)GRѵ� ����Z@b�����&a�j%-��?@I��l�s��x�      U   {   x�ss�1202�tK��Q(N�M-.I-R�OS �*� 'X����������if`aajdr���e�Kss�Zq�3������� l��P?ЄpO?�lxf^	a�-�M�n0�0777� ����� "�.      V   �   x��ϻ
1��ޯ�Hޏ^DVD[���E�@�o@k7�Lu����D�9���QV9N)E�O�_Zf�2�p����9�ʶtRk+m۔0���Ã���_j'�v�I���)����.(F����C	3�Ƶ.}?��Q4M�t'cN�'�+��?�JKi      W   �   x���M� F��)\t�E#���7hb�`�0@b�}�� ��{/���+jQW�Ƕ���x���la�#*��G��)tf�:�q5������B��ۃsʇ+�딎��A�t��d�w8�Ʋ_��H��h�9iH'��+���z�k�y��ה>����^ �|v	     