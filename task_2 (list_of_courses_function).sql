DROP FUNCTION IF EXISTS list_courses_for_lecturer_and_semester;


CREATE FUNCTION list_courses_for_lecturer_and_semester(lecturer_name varchar(45), input_semester_id varchar(45))
RETURNS text AS $$

	SELECT array_to_string(array_agg(course_id), ', ')
	FROM schedule_of_classes
	LEFT JOIN lecturers ON schedule_of_classes.lecturer_id = lecturers.lecturer_id
	WHERE name = lecturer_name AND semester_id = input_semester_id;
    
$$ LANGUAGE SQL;

