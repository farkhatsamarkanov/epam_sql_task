SELECT 
    courses.course_title AS "Course title", 
    count(distinct schedule_of_classes.student_id) AS "Number of Students",
	count(distinct schedule_of_classes.lecturer_id) "Number of Lecturers",
	course_maxlecturerrank_lecturer_rankname.name AS "Highest lecturer rank",
	course_inactiveUsersNumber.count AS "Inactive users",
	t.semester_id AS "Most picked semester"
FROM courses
	LEFT JOIN schedule_of_classes 
		ON schedule_of_classes.course_id = courses.course_id
	LEFT JOIN
		course_maxlecturerrank_lecturer_rankname 
		ON courses.course_id = course_maxlecturerrank_lecturer_rankname.course_id
	LEFT JOIN course_inactiveUsersNumber
		ON courses.course_id = course_inactiveUsersNumber.course_id
	LEFT JOIN get_most_picked_semesters() as t
		ON courses.course_id = t.course_id
GROUP BY course_title, "Highest lecturer rank", "Inactive users", "Most picked semester"